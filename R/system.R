#' Download a file if it exists, and if it's a zip file, unzip it.
#'
#' Often spatial data is delivered as a .zip file, and it's pretty common to
#' have to unzip it right away. Further, when running a script it may be that
#' the file is already downloaded, so there is no need to download it again if
#' so.
#' @usage dl_file(link)
#' @param link character
#' @return NA
#' @keywords utilities
#' @export
#' @examples
#' dl_file("http://www2.census.gov/geo/tiger/GENZ2017/shp/cb_2017_us_county_500k.zip")
#'
dl_file <- function(link) {
  ## check to see if the file exists
  if (file.exists(basename(link))) {

    cat("File already exists; not downloading")
  } else {

    download.file(link, destfile=basename(link), mode = "wb")

    ## if it's a zip file, unzip it
    if (grepl(".zip", basename(link))) {

      unzip(basename(link))
      cat("File unzipped")
    }
  }
}


#' Create a random-ish name for a file
#'
#' Sometimes it's necessary to generate unique names of files, like when
#' multiple users may be submitting something at around the same time (sys.time
#' would probably work, but no guarantee). This function uses paste0 to put
#' together the prefix, sys.time as a numeric, three letters, and the extension
#' (specified by user, .csv otherwise).
#'
#' @usage rand_name(prefix = "hometown-map-", ext = ".csv")
#' @param prefix character
#' @param ext character
#' @return name
#' @keywords utilities, system
#' @export
#' @examples
#' write.csv(df, file = rand_name(prefix = "hometown-map-", ext = "csv")), row.names = FALSE)
#'
rand_name <- function(prefix = "", ext = ".csv") {
  ## construct name based on prefix, systime, letters, and ext
  name <- paste0(prefix,
                 as.numeric(Sys.time())*100000,
                 "-", sample(letters, 1),
                 sample(letters, 1),
                 sample(letters, 1),
                 ext)
}

#' Build blogdown site without rendering .Rmd files unless it's necessary
#'
#' Only generate .html files from .Rmd if (1) the .Rmd has been edited since the
#' last render or (2) the corresponding .html doesn't exist yet
#'
#' @usage build_custom(dir = getwd())
#' @param dir character string of the current directory
#' @return NA
#' @keywords system, utilities, blogdown
#' @examples
#' write.csv(df, file = rand_name(prefix = "hometown-map-", ext = "csv")), row.names = FALSE)
#'
build_custom <- function(dir = getwd()) {
  ## set working directory
  setwd(dir)

  ## get vector of .Rmd files
  rmd.files <- list.files("content", "\\.Rmd$", recursive = TRUE, full.names = TRUE)

  ## get vector of .html files
  html.files <- sub("\\.Rmd$", ".html", rmd.files)

  ## get list of directories
  content.dirs <- list.dirs("content")

  ## get vector of logicals for whether or not the file needs knitting
  needs.knitted <- !file.exists(html.files) | utils::file_test("-ot", html.files, rmd.files)

###### deal with temporary files
  ## remove everything in ~/tmp directory
  ## TODO name this something else
  unlink("~/tmp-blogdown-build/*", recursive = TRUE)

  ## make directories mimicking content style structure
  ## TODO don't do this with a loop
  for (i in 1:length(content.dirs)) {
    dir.create(paste0("~/tmp-blogdown-build/", content.dirs)[i], showWarnings = TRUE, recursive = TRUE)
  }

##### hide files that don't need rendering
  ## copy so date can be preserved
  ## TODO don't do this with a loop
  for (i in 1:length(rmd.files[!needs.knitted])) {
    file.copy(rmd.files[!needs.knitted][i],
              paste0("~/tmp-blogdown-build/", dirname(rmd.files[!needs.knitted][i])),
              copy.date = TRUE)
  }

  ## then remove
  unlink(rmd.files[!needs.knitted])

##### build the site; no need for custom function
  blogdown::build_site()

  ## unhide files
  rmd.files.tmp <- list.files("~/tmp-blogdown-build/content", "\\.Rmd$", recursive = TRUE, full.names = TRUE)

  rmd.files.new <- sub("/home/matt/tmp-blogdown-build/", "", rmd.files.tmp)

  for (i in 1:length(rmd.files.tmp)) {
    file.copy(rmd.files.tmp[i], rmd.files.new[i], copy.date = TRUE)
  }

  ## remove everything in ~/tmp directory
  ##unlink("~/tmp/*", recursive = TRUE)
}
