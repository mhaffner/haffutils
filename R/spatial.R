#' Clip a raster
#'
#' There's not a raster equivalent of a vector clip, so this combines crop
#' (which gets a rectangular area) and mask (which clips but does not alter
#' extents appropriately)
#'
#' @usage raster_clip
#' @param raster.data raster dataset
#' @param clip.region spatial dataset
#' @return raster dataset
#' @export
#' @keywords spatial
#' @examples
#' raster_clip(wisconsin.dem, buffalo.county)
raster_clip <- function(raster.data, clip.region) {
  ## get rectangular area
  raster::crop(raster.data, clip.region) %>%
    ## mask it
    raster::mask(., clip.region)
}

#' Simple usage of Moran's I with a symmetric set of n neighbors and an sf
#' object
#'
#' The function moran.test (from spdep) only works with sp objects and requires
#' a listw object as an argument. To simplify this for the WLIA workshop, I've
#' created a simple function that will convert from sp to sf and create the
#' symmetric listw object behind the scenes.
#'
#' @usage moran_test(sf.obj, var, knn)
#' @param sf.obj sf object
#' @param var variable name (character string)
#' @param knn number of neighbors (numeric)
#' @return object created by the function moran.test
#' @export
#' @keywords spatial
moran_test <- function(sf.obj, var, knn, longlat=FALSE) {
  ## convert from sp to sf for spdep package requirements
  sp.obj <- as(sf.obj, "Spatial")

  ## create neighbors object
  listw.obj <- sp.obj %>%
    sp::coordinates() %>%
    spdep::knearneigh(., k = knn, longlat) %>%
    spdep::knn2nb() %>%
    spdep::make.sym.nb() %>%
    spdep::nb2listw()

  ## simple wrapper around moran.test for the sake of the workshop
  result <- spdep::moran.test(sp.obj@data[[var]],
                              listw.obj)

  return(result)
}

#' Use of Moran's I with multiple symmetric sets of neighbors and an sf object
#'
#' The function moran.test (from spdep) only works with sp objects and requires
#' a listw object as an argument. To simplify this for the WLIA workshop, I've
#' created a simple function that will convert from sp to sf and create the
#' symmetric listw object behind the scenes.
#'
#' @usage moran_test(sf.obj, var, min.neigh, max.neigh, interval)
#' @param sf.obj sf object
#' @param var variable name (character string)
#' @param min.neigh minimum number of neighbors (numeric)
#' @param max.neigh maximum number of neighbors (numeric)
#' @param interval number to increment by (numeric)
#' @return data frame of results of multiple Moran's I tests
#' @export
#' @keywords spatial
moran_iter <- function(sf.obj, var, min.neigh, max.neigh, interval, longlat){

  ## prepare data frame
  moran.results <- data.frame(matrix(NA,
                                     nrow=length(seq(min.neigh, max.neigh, interval)),
                                     ncol=4))

  ## rename columns
  colnames(moran.results) <- c("neighbors", "morans_i", "variance", "p.value")

  ## vector of number of neighbors based on user input
  neigh.list <- seq(min.neigh, max.neigh, interval)

  ## iterate and save results
  for (i in 1:length(neigh.list)){
    result <- moran_test(sf.obj, var, i)
    moran.results$neighbors[i] <- neigh.list[i] # number of neighbors
    moran.results$morans_i[i] <- result$estimate[1] # moran's i stat
    moran.results$variance[i] <- result$estimate[3] # variance
    moran.results$p.value[i] <- result$p.value # p.value
  }
  return(moran.results)
}

#' Compute the centroid of an sf dataset in one dimension
#'
#' This function is used to retrieve either the 'x' or the 'y' coordinates of a
#' centroid. It is intended for use with polygons but works as a nice utility
#' for points as well. Essentially it "scrapes" the 'x' component of the
#' object's geometry. With polygons this should only be done in a projected CRS.
#' Appropriate warnings from sf will be displayed either way.
#'
#' @param sf_obj sf object
#' @param dim dimension for calculating the centroid (either x or y, case insensitive
#' @return vector of the centroids
#' @export
#'
centroid_dim <- function(sf_obj, dim = 'X') {
  dim <- sf::st_centroid(sf_obj) %>%
    sf::st_coordinates() %>%
    data.frame() %>%
    dplyr::pull(toupper(dim))

  return(dim)
}

#' Compute the mean center or weighted mean center
#'
#' Compute the mean center of points or polygons. An optional variable can be
#' supplied to compute the weighted mean center. If a weighted mean center is
#' desired and/or polygons are used, the data ought to use a projected CRS
#'
#' @param sf_obj object to compute mean centers on (of the class sf)
#' @param var (optional) variable to use in computing a weighted mean center
#' @return sf object (point)
#' @export
#'
mean_center <- function(sf_obj, var = NULL) {

  ## TODO allow for multiple variables supplied as a vector of values
  center_x <- centroid_dim(sf_obj, 'X')

  center_y <- centroid_dim(sf_obj, 'Y')

  if (!is.null(var)) {
    mean_center_x <- sum(sf_obj[[var]] * center_x) / sum(sf_obj[[var]])
    mean_center_y <- sum(sf_obj[[var]] * center_y) / sum(sf_obj[[var]])
  } else {
    mean_center_x <- mean(center_x)
    mean_center_y <- mean(center_y)
  }

  point <- sf::st_as_sf(data.frame(x = mean_center_x,
                                   y = mean_center_y),
                        coords = c("x", "y"),
                        crs = st_crs(sf_obj)) %>%
    mutate(var = var)

  return(point)
}
