#' Create a simple, nice looking histogram using a vector as input
#'
#' The built-in hist() function is nice because it takes vectors, but it
#' produces ugly plots. The ggplot2 function geom_histogram() produces very nice
#' looking plots but requires a dataframe, a few more lines of code than I want
#' students to have to write, and it defaults to 30 bins. This function uses
#' Sturges's method to determine the number of bins.
#' @usage pretty_hist(x)
#' @param x numeric vector
#' @param title character string, optional title
#' @param xlab character string, optional x-axis label
#' @param ylab character string, optional y-axis label
#' @param ... other arguments passed to geom_histogram
#' @return a plot
#' @keywords visualization
#' @export
#' @examples
#' pretty_hist(rnorm(1000))
#'
pretty_hist <- function(x, title="", xlab="", ylab="Frequency", ...) {
  ## get ellipsis arguments
  args <- list(...)

  ## remove na values
  x <- x[!is.na(x)]

  ## set some reasonable defaults for geom_histogram (ellipsis arguments)
  if (!"fill" %in% names(args)) {
    args$fill <- viridis::viridis(100) %>% sample(1)
  }

  if (!"alpha" %in% names(args)) {
    args$alpha <- 0.80
  }

  ## breaks using sturges
  breaks <- pretty(range(x),
                   n = nclass.Sturges(x),
                   min.n = 1)

  args$breaks <- breaks

  ## set up histogram
  hist <- do.call(ggplot2::geom_histogram, args)

  ## create data frame for ggplot2
  df <- data.frame(x=x)

  ## create hist
  ggplot2::ggplot(df, ggplot2::aes(x=x)) +
    hist +
    ggplot2::ggtitle(title) +
    ggplot2::ylab(ylab) +
    ggplot2::xlab(xlab)
}

#' Create a simple, nice looking density plot using a vector as input
#'
#' Base R does not have a one line/one function option for creating density
#' plots. Similar to pretty_hist(), this function takes a vector as input and
#' produces a nice looking density plot of a single variable using ggplot2 under
#' the hood.
#' @usage pretty_dens(x)
#' @param x numeric vector
#' @param title character string, optional title
#' @param xlab character string, optional x-axis label
#' @param ylab character string, optional y-axis label
#' @param ... other arguments passed to geom_density
#' @return a plot
#' @keywords visualization
#' @export
#' @examples
#' pretty_dens(rnorm(1000))
#'
pretty_dens <- function(x, title="", xlab="", ylab="Density", ...) {
  ## remove na values
  x <- x[!is.na(x)]

  ## get ellipsis arguments
  args <- list(...)

  ## set some reasonable defaults for geom_histogram (ellipsis arguments)
  if (!"fill" %in% names(args)) {
    args$fill <- viridis::viridis(100) %>% sample(1)
  }

  if (!"alpha" %in% names(args)) {
    args$alpha <- 0.65
  }

  dens <- do.call(ggplot2::geom_density, args)

  ## create data frame for ggplot2
  df <- data.frame(x=x)

  ggplot2::ggplot(df, ggplot2::aes(x=x)) +
    dens +
    ggplot2::ggtitle(title) +
    ggplot2::ylab("Density") +
    ggplot2::xlab("")
}

#' Visualize results from pnorm.
#'
#' Suppose you want to calculate the area under a normal distribution based on a
#' certain z-score or raw value. After using pnorm(), you many want to visualize
#' the results for the sake of verification. You can simply take your inputs to
#' pnorm, substitute the function name with pnorm_vis and it will create a
#' visualization. Additionally, you can supply two values (q and q2) and it will show the area between two z-scores or raw values
#'
#' @usage pnorm_vis(q)
#' @param q numeric value representing a z-score (or a raw value)
#' @param mean numeric, mean
#' @param sd numeric, standard deviation
#' @param lower.tail: logical; if TRUE (default), probabilities are P[X <= x];
#'   otherwise, P[X > x].
#' @param ... other arguments passed to ggplot2's stat_function function
#' @param q2 a second numeric value to use in tandem with q; if both are used,
#'   the function will produce the area under the curve
#' @return a plot
#' @keywords visualization
#' @export
#' @examples
#' pnorm_vis(1.5)
#' pnorm_vis(1.5, lower.tail = FALSE)
#' pnorm_vis(q = -1.2, q2 = 1.7)
#' pnorm_vis(q = 20, mean = 0, sd = 40, lower.tail = FALSE)
#'
pnorm_vis <- function(q, mean = 0, sd = 1, lower.tail = TRUE, ..., q2 = NULL) {

  args <- list(...)

  if (is.null(q2)) {
    q1 <- -5
    q2 <- (q - mean)/sd
  } else {
    q1 <- (q - mean)/sd
  }

  #if (!"fill" %in% names(args)) {
  #  args$fill <- viridis::viridis(100) %>% sample(1)
  #}
  if("lower.tail" %in% names(args) & !is.null(q2)) {
    warning("Using 'lower.tail' with q2 is not meaningful. lower.tail will be ignored.")
  }

  if (lower.tail == FALSE) {
    q1 <- (q - mean)/sd
    q2 <- 5
  }

  xlim_obj <- c(q1, q2)

  if (!"alpha" %in% names(args)) {
    args$alpha <- 0.50
  }

  if (!"fill" %in% names(args)) {
    args$fill <- viridis::viridis(100) %>% sample(1)
  }

  if (mean != 0 | sd != 1) {
    theme_dens <- ggplot2::theme(axis.text.x=ggplot2::element_blank())
  } else {
    theme_dens <- ggplot2::theme()
  }

  args$fun <- dnorm
  args$xlim <- c(xlim_obj[1],xlim_obj[2])
  args$geom <- "area"

  shaded_area <- do.call(ggplot2::stat_function, args)

  ggplot2::ggplot(data.frame(x = c(-5, 5)), ggplot2::aes(x)) +
    ggplot2::stat_function(fun = dnorm) +
    shaded_area +
    theme_dens
}

#' Visualize results from qnorm.
#'
#' This allows for visualizing results from qnorm(). For the purpose of
#' visualization, mean and sd are not terribly meaningful; only p and lower.tail
#' are necessary here.
#'
#' @usage qnorm_vis(q)
#' @param p probability, i.e. percentage of data under the curve of a normal distribution
#' @param lower.tail: logical; if TRUE (default), probabilities are P[X <= x];
#'   otherwise, P[X > x].
#' @param ... other arguments passed to ggplot2's stat_function function
#' @return a plot
#' @keywords visualization
#' @export
#' @examples
#' qnorm_vis(0.75)
#' qnorm_vis(0.35, lower.tail = FALSE)
#'
qnorm_vis <- function(p, lower.tail = TRUE, ...) {

  args <- list(...)

  q1 <- qnorm(p)

  if (lower.tail == FALSE) {
    q1 <- qnorm(1-p)
    q2 <- 5
  } else {
    q2 <- -5
  }

  xlim_obj <- c(q1, q2)

  if (!"alpha" %in% names(args)) {
    args$alpha <- 0.50
  }

  if (!"fill" %in% names(args)) {
    args$fill <- viridis::viridis(100) %>% sample(1)
  }

  args$fun <- dnorm
  args$xlim <- c(xlim_obj[1],xlim_obj[2])
  args$geom <- "area"

  shaded_area <- do.call(ggplot2::stat_function, args)

  ggplot2::ggplot(data.frame(x = c(-5, 5)), ggplot2::aes(x)) +
    ggplot2::stat_function(fun = dnorm) +
    shaded_area
}
